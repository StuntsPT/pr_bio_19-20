# Assignment 01

### Deadline:

December 6th 2019


### Format:

PDF


### Delivery:

E-mail


## Introduction

During the last few classes you have learned to create new Virtual Machines (VMs), and to install a GNU/Linux based Operating System (OS).
Furthermore you have learned how to navigate a \*NIX shell, and use some of it's programs.
You will have to use these new skills combined with some **research** to complete your assignment.


## Objectives

* Create a new VM, and install a new GNU/Linux based OS on it that is **not** *Ubuntu* or one of it's close derivatives (*kubuntu*, *lubuntu*, *xubuntu*, etc..).
* This new OS should use at least 2 partitions - on for `/` and one for `/home`. You may add more if you like.
* Find and provide information about the kernel your OS is using (Take a screenshot).
* Find the configuration file where partition filesystem mount points are stored and report it.
* Provide a screenshot of the desktop.
* Install the program `htop`, run it and provide a screenshot.
* Export your VM as an appliance and get the SHA256SUM value of the generated `.ova` file. [Here](https://superuser.com/a/898377) is one way to generate this hash on Windows. But there are alternative ways.
* Bonus points if you provide a **brief** explanation on what each of these are.

## Methodology

* You can use any text processing software to write your report.
  * Any text files you wish to present should be presented as "preformatted text", like in the example below:

`/etc/asound.conf`

```bash
# Use PulseAudio by default
pcm.!default {
  type pulse
  fallback "sysdefault"
  hint {
    show on
    description "Default ALSA Output (currently PulseAudio Sound Server)"
  }
}

ctl.!default {
  type pulse
  fallback "sysdefault"
}

# vim:set ft=alsaconf:
```

* Your report should have 2 sections:
    * Introduction - where you describe the GNU/Linux distro you have chosen (provide a logo too), and a short sentence on its History. Make sure you mention which package manger is used, and the respective package format.
    * Results - where you describe what is requested in the objectives.
* The report can either be provided in English or Portuguese. It's your choice.
* Don't forget to include a title and a cover page!
* Do not forget to include the SHA256SUM hash for the `.ova` file in the report.
* Keep your `.ova` file for a while since some will be randomly requested.
* Every figure in the report **has to be referenced** in the text!
* Make sure you proofread your work. You only get one shot.
