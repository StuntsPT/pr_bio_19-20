# Assignment 02


### Deadline:

January 24th 2020


### Format:

Text files


### Delivery:

`git`


## Introduction

You have now learned to use a version control system (VCS) - `git`. Your second task is based on it.
This is a very practical task, and the work flow you will have to master for it will likely be useful during the rest of your career in bioinformatics.
A decent grip on \*NIX shell is a big help for this task.

The words "firstname" and "lastname" represent your given name and family name respectively for the rest of this assignment.


## Objectives

1. *Hand-made* sequences
  * Rename the file you submitted as your first homework (from **classes[1]**) as `firstname_lastname_handmade_sequences.fasta`
  * If you feel you can improve this homework, now is you last chance to correct it
    * In case you make any changes to your original sequences, please rename the file `firstname_lastname_handmade_sequences_corrected.fasta` instead

2. Reference mapping data
  * Name the files you created for your second homework (from **classes[5]**) as `firstname_lastname_commands.sh` and `firstname_lastname_results.txt` (the commands and results files respectively)

3. DNA alignments
  * Download between 20 and 50 sequences of a species (or genera) of your choice, but make sure all sequences are from the same gene fragment, from a DNA sequence database
  * Align your DNA sequences using an alignment software of your choice
  * Name your "original" and "aligned" files `firstname_lastname_sequences.fasta` and `firstname_lastname_aligned.fasta` respectively

4. `gitting` it out
  * Consider the following `git` [repository](https://github.com/StuntsPT/pr_bio_2019-2020_assignments)
  * File a new issue on the repository, where you will state that your name is missing from the `students.csv` file.
  * Add the missing data to `students.csv`
    * "Number" represents your student number
    * "Name" represents your full name
    * Do not make any other changes to the file
  * Commit these changes with an appropriate message
  * Add the files mentioned in steps 1,2 and 3 to the repository in your account
  * Get your changes accepted into the original repository


## Methodology

1. Handmade sequences
  * Methodology is described [here](https://stuntspt.gitlab.io/pr_bio_2018-2019/classes/class02.html) in the last slide of the presentation

2. Reference mapping data
  * Methodology is described [here](https://stuntspt.gitlab.io/pr_bio_2018-2019/classes/class06.html) in the last slide of the presentation

3. DNA alignments
  * Methodology for this task is detailed [here](https://stuntspt.gitlab.io/pr_bio_2018-2019/classes/class06.html) in the slides named "Obtaining Sequences" and "Aligning Sequence Data"

4. `gitting` it out
  * Use github's interface to open a new issue in the repository
  * Fork [the repository](https://github.com/StuntsPT/pr_bio_2019-2020_assignments) under your own account
  * Fork the repository into your own account
  * Consider the directory `generated_seqs`
    * Add **one** of the following files to this directory:
      * `firstname_lastname_handmade_sequences.fasta`
      * `firstname_lastname_handmade_sequences_corrected.fasta`
  * Commit this change to your repository

  * Consider the directory `reference_mapping`
    * Add two files to this directory:
      * `firstname_lastname_commands.sh`
      * `firstname_lastname_results.txt`
  * Commit this change to your repository with an appropriate message

  * Consider the directory `dna_alignments`
    * Add two files to this directory:
      * `firstname_lastname_sequences.fasta`
      * `firstname_lastname_aligned.fasta`
  * Commit this change to your repository

  * Details on this task are available [here](https://stuntspt.gitlab.io/pr_bio_2018-2019/classes/class07.html) from the slide named "Getting (literally) started with git" to the end of the presentation

  * Once all this is done, open a "pull request" so that your changes can be included under the main repository
  * Close your issue once the changes are accepted
