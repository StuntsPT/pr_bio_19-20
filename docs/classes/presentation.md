### classes[0] = "Presentation"

#### Perspetivas em bioinformática 2019-2020

![Logo EST](presentation_assets/logo-ESTB.png)

<center>Francisco Pina Martins</center>

<center>[@FPinaMartins](https://twitter.com/FPinaMartins)</center>

---

## Practical info

Schedule: Fridays - 13:30 to 15:30 **Room S 0.01**

Questions? f.pina.martins@estbarreiro.ips.pt <!-- .element: class="fragment" data-fragment-index="1" -->

---

## Avaliação

<ul>
<li class="fragment"><font color="orange">Contínua</font> **OU** <font color="deeppink">exame</font></li>
<ul>
<li class="fragment"><font color="deeppink">1ª e/ou 2ª época</font/></li>
<li class="fragment"><font color="orange">Avaliação contiunua e/ou 2ª época</font/></li>
<ul>
<li class="fragment"><font color="orange">20% - Contexto sala de aula + "homework"</font/></li>
<li class="fragment"><font color="orange">30% - 1º trabalho</font/></li>
<li class="fragment"><font color="orange">30% - 2º trabalho</font/></li>
<li class="fragment"><font color="orange">20% - teste</font/></li>
<li class="fragment"><font color="orange">Sem notas mínimas</font/></li>
</ul>
<li class="fragment">Quem realizar 3/4 das componentes da avaliação contínua, não pode aceder à 1ª época de exame</font/></li>
</ul>
</ul>

---

## What is "bioinformatics"?

* There really is no consensus on the subject <!-- .element: class="fragment" data-fragment-index="1" -->
* Loose definitions <!-- .element: class="fragment" data-fragment-index="2" -->

|||

## Such as?

<ul>
<li class="fragment">Develop methodologies and analyses tools to <font color="red">-----------</font> biological data</li>
<ul>
 <li class="fragment"><font color="orange">explore</font></li>
 <li class="fragment"><font color="green">store</font></li>
 <li class="fragment"><font color="cyan">organize</font></li>
 <li class="fragment"><font color="purple">systematize</font></li>
 <li class="fragment"><font color="yellow">annotate</font></li>
 <li class="fragment"><font color="navy">visualize</font></li>
 <li class="fragment"><font color="grey">query</font></li>
 <li class="fragment"><font color="deeppink">mine</font></li>
 <li class="fragment"><font color="#009933">understand</font></li>
 <li class="fragment"><font color="#FFCC66">interpret</font></li>
</ul>
</ul>

|||

## How do we get there?

* Computer science <!-- .element: class="fragment" data-fragment-index="1" -->
* Statistics <!-- .element: class="fragment" data-fragment-index="2" -->
* Mathematics <!-- .element: class="fragment" data-fragment-index="3" -->
* Biology <!-- .element: class="fragment" data-fragment-index="4" -->

<p class="fragment">([Searls 2010](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1000809); [Abdurakhmonov 2016](https://www.intechopen.com/books/bioinformatics-updated-features-and-applications/bioinformatics-basics-development-and-future))</p>

---

## Overview

* DNA and proteins <!-- .element: class="fragment" data-fragment-index="1" -->
* Virtual Machines <!-- .element: class="fragment" data-fragment-index="2" -->
* GNU/Linux command line <!-- .element: class="fragment" data-fragment-index="3" -->
* Digital DNA sequences <!-- .element: class="fragment" data-fragment-index="4" -->
* Short intro to programming <!-- .element: class="fragment" data-fragment-index="5" -->
* Alternate perspectives seminars <!-- .element: class="fragment" data-fragment-index="6" -->

---

## My own background

<ul>
<li class="fragment">Graduation in Marine Biology (2004)</li>
<center><p class="fragment">![dolphin](presentation_assets/dolphin.jpg)</p></center>
<li class="fragment">MSc in "Evolutionary and Developmental Biology" (2006)</li>
<li class="fragment">PhD in "Global Change Biology and Ecology" (2018)</li>
</ul>

|||

## Wait, what?

* First contact during MSc <!-- .element: class="fragment" data-fragment-index="1" -->
    * PERL <!-- .element: class="fragment" data-fragment-index="2" -->
    * Sequence data <!-- .element: class="fragment" data-fragment-index="2" -->
    * GNU/Linux <!-- .element: class="fragment" data-fragment-index="2" -->
* Research fellowships <!-- .element: class="fragment" data-fragment-index="3" -->
    * Python <!-- .element: class="fragment" data-fragment-index="4" -->
    * Sysadmin <!-- .element: class="fragment" data-fragment-index="4" -->
    * Software management <!-- .element: class="fragment" data-fragment-index="4" -->
* PhD <!-- .element: class="fragment" data-fragment-index="5" -->
    * Reproducibility <!-- .element: class="fragment" data-fragment-index="6" -->
    * Automation <!-- .element: class="fragment" data-fragment-index="6" -->
    * Performance <!-- .element: class="fragment" data-fragment-index="6" -->

---

## What about you?

---

## References

* [Searls 2010](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1000809);
* [Abdurakhmonov 2016](https://www.intechopen.com/books/bioinformatics-updated-features-and-applications/bioinformatics-basics-development-and-future)

