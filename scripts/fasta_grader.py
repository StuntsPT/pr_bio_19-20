#!/usr/bin/env python3

# Usage: python3 fasta_grader.py file.fasta [MAX_SCORE]

from sys import argv
from itertools import product
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC


def fasta_parser(filename_to_grade):
    """
    Parses a FASTA formatted file and returns a dict with {sequence_name:
    sequence, ...}
    """
    infile = open(filename_to_grade, "r")
    sequence_data = {}
    count = 0
    datatypes = [IUPAC.IUPACAmbiguousDNA(), IUPAC.IUPACUnambiguousDNA(),
                 IUPAC.IUPACUnambiguousDNA(), IUPAC.IUPACUnambiguousDNA(),
                 IUPAC.IUPACUnambiguousDNA(), IUPAC.IUPACUnambiguousDNA(),
                 IUPAC.IUPACUnambiguousDNA(), IUPAC.IUPACProtein()]
    for lines in infile:
        if lines.startswith(">"):
            seqname = lines.strip()[1:]
            sequence_data[seqname] = Seq("", datatypes[count])
            count += 1
        elif lines.startswith("\n"):
            pass
        else:
            sequence_data[seqname] += lines.strip().upper()

    return sequence_data


def auto_grader(sequence_data, score=100, seq_length=21):
    """
    Grades the homework, take a sequence dictionary as input, returns a score
    Default starting score is 100, every error will subtract from this
    Score should start lower for resubmissions, or late submissions
    """
    if any([len(x) != seq_length for x in list(sequence_data.values())[:-1]]):
        score += -10
        print("Not all sequences are %s bp long!" % seq_length)


    def _correct_encoding(seq, data_type):
        """
        Checks if all letters are valid DNA codes
        Takes a sequence as input and returns the score difference
        """
        if any([x not in data_type for x in seq]):
            print("Some bases are not correctlly encoded:\n%s" % seq)
            minus = -10
        else:
            minus = 0

        return minus


    def _extend_ambiguous_dna(seq):
        """
        return list of all possible sequences given an ambiguous DNA
        input
        Taken from https://stackoverflow.com/a/27552377/3091595
        """
        ambigs = IUPAC.IUPACData.ambiguous_dna_values
        return list(map("".join, product(*map(ambigs.get, seq))))


    seqlist = list(sequence_data.keys())

    # Ambiguous sequence
    ambig_seq = sequence_data[seqlist[0]]
    # Are all letters valid DNA?
    score += _correct_encoding(ambig_seq, IUPAC.IUPACData.ambiguous_dna_values)
    # Are there 4 ambiguities?
    ambig_bases = sum([ambig_seq.count(x) for x in
                       IUPAC.IUPACData.ambiguous_dna_letters[4:]])
    ambig_penalty = -abs(ambig_bases - 4) * 3
    score += ambig_penalty
    if ambig_penalty != 0:
        print("Sequence %s does not have 4 ambiguities!" % ambig_seq)
    # Generate all possible disambiguations:
    disambiguations = _extend_ambiguous_dna(ambig_seq)

    # Disambiguation
    for disamb in seqlist[1:4]:
        score += _correct_encoding(sequence_data[disamb],  # Valid DNA?
                                   IUPAC.IUPACData.unambiguous_dna_letters)
        if sequence_data[disamb] not in disambiguations:
            score += -10
            print("Sequence %s is not a disambiguation of %s." %
                  (sequence_data[disamb], ambig_seq))

    # Reverse
    rev_seq = sequence_data[seqlist[4]]
    score += _correct_encoding(rev_seq,  # Valid DNA?
                               IUPAC.IUPACData.unambiguous_dna_letters)
    if rev_seq[::-1] not in [sequence_data[x] for x in seqlist[1:4]]:
        score += -10
        print("Sequence %s is not the reverse of any of the disambiguations." %
              rev_seq)

    # Complement
    comp_seq = sequence_data[seqlist[5]]
    score += _correct_encoding(comp_seq,  # Valid DNA?
                               IUPAC.IUPACData.unambiguous_dna_letters)
    if comp_seq.complement() not in [sequence_data[x] for x in seqlist[1:4]]:
        score += -10
        print("Sequence %s is not the complement of any of the disambiguations." %
              comp_seq)

    # Rev Comp
    revcomp_seq = sequence_data[seqlist[6]]
    score += _correct_encoding(revcomp_seq,  # Valid DNA?
                               IUPAC.IUPACData.unambiguous_dna_letters)
    if revcomp_seq.reverse_complement() not in [sequence_data[x] for x in
                                                seqlist[1:4]]:
        score += -10
        print("Sequence %s is not the reverse-complement of any of the "
              "disambiguations." % revcomp_seq)

    # Translation
    translation = sequence_data[seqlist[7]]
    score += _correct_encoding(translation,  # Valid Protein?
                               IUPAC.IUPACData.protein_letters)
    if translation not in [sequence_data[x].translate() for x in seqlist[1:4]]:
        score += -10
        print("Sequence %s is not the translation of any of the "
              "disambiguations." % translation)

    return score


if __name__ == "__main__":
    try:
        SEQUENCES = fasta_parser(argv[1])
    except:
        quit("This is no FASTA! It has to be manually graded...")
    if len(argv) == 2:
        FINAL_SCORE = auto_grader(SEQUENCES)
    else:
        FINAL_SCORE = auto_grader(SEQUENCES, int(argv[2]))
    print("Final score is: %s/%s" % (FINAL_SCORE, argv[2]))
